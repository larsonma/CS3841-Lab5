/**
 * This file implements the IO bound child process which reads data from a binary sound file. The data is forwarded
 * to a seperate child process to preprocess the data before sending it to the audio card.
 * @file FileReader.c
 * @author Gunther Huebler, Mitchell Larson
 * @date 10/11/2017
 */


#define EXIT_ERROR (1)
#define EXIT_SUCCESS (0)
#define BYTE (1)

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "AudioInterface.h"
#include "mainPlayBack.h"
#include <stdint.h>

/**
 * Reads in a binary audio file in chunks of 2048 bytes. As data is read, it is sent through a pipe to a seperate child
 * process that will perform volume adjustments and send the pipe to the audio player architecture. The number of bytes
 * read is printed to the console every time the file is read. This function will kick off the audio playing sequence and
 * run until the entire is read.
 * @param fileNameToRead	non-null pointer to a fileName
 * @param pipeToSendTo		The pipe to send audio data through
 */
void readFile(char* fileNameToRead, int pipeToSendTo) {
	//check that the file name is not null
	if (fileNameToRead != NULL) {
		// Allocate a buffer so that we can read data in from the file.  Initialize it to all zeros.
		char *buffer = (char*) malloc(BUFFER_SIZE);
		memset(&buffer[0], 0, BUFFER_SIZE);

		// Open the file that is going to be read.
		FILE* filedesc;
		if((filedesc = fopen(fileNameToRead, "rb"))==NULL) {
			printf("There was an issue opening the file - %s\r\n", fileNameToRead);
			exit(EXIT_ERROR);
		}

		int bytesRead = 0;
		int rc = 1;

		do {
			// Read from the file.
			rc = fread(buffer, BYTE, BUFFER_SIZE, filedesc);
			bytesRead += rc;
			printf("Total Bytes Read: %d\n", bytesRead);

			// If we have read anything, write it out to the audio device.
			if (rc > 0) {

				write(pipeToSendTo, buffer, BUFFER_SIZE);
				write(pipeToSendTo, &rc, sizeof(int));

				// Zero out the buffer.
				memset(&buffer[0], 0, BUFFER_SIZE);
			}
		} while (rc > 0);

		close(pipeToSendTo);

		free(buffer);

		exit(EXIT_SUCCESS);
	}

}


