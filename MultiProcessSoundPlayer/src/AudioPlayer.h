/**
 * This file contains the interface for the AudioPlayer file. These set of functions provide the ability to send
 * a stream of audio data to ALSA archecture after adjusting the volume.
 * @file AudioPlayer.h
 * @author Mitchell Larson, Gunther Huebler
 * @date 10/11/2017
 */

#ifndef SRC_AUDIOPLAYER_H_
#define SRC_AUDIOPLAYER_H_

/**
 * Adjusts the volume of 16 bit chunks of data in buffer pointer. Loops through buffer shifting the bits
 * to the left or right based on the gain. This is equivalent to taking each 16 bit chunk, and multiplying
 * it by 2^gain
 * @param bufferIn - buffer to be read from
 * @param bufferLength - length of the buffer
 * @param gain - and integer representing the volume adjustment
 */
void* adjustGain(void*, int, int );

/**
 * Initializes audio player to play sound. Sound information is read in from a pipe, send to
 * adjustGain where it increases or decreases the volume. The adjusted buffer is then used to send
 * data to the audio card.
 * @param volumeAdjustment - Volume adjustment. Valid range is -4 to 4
 * @param pAudioDeviceName - Name of device to play audio on
 * @param pipeToReadFrom - pipe identifier to read from
 */
void playAudio(int, char*, int );

#endif /* SRC_AUDIOPLAYER_H_ */
