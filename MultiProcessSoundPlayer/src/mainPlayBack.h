/**
 * This file contains a BUFFER_SIZE that is used in file to specify the buffer size.
 * @file mainPlayBack.h
 * @author Mitchell Larson, Gunther Huebler
 * @date 10/11/2017
 */
#ifndef SRC_MAINPLAYBACK_H_
#define SRC_MAINPLAYBACK_H_

#define BUFFER_SIZE (2048)

#endif /* SRC_MAINPLAYBACK_H_ */
