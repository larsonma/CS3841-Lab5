/**
 * This file contains the source code for the CPU bound child process. Audio data will be read from a pipe being written to
 * in the IO bound process. The 2 functions in this file adjust the gain of incoming data and then send the data to the
 * ALSA architecture.
 * @file AudioPlayer.c
 * @author Gunther Huebler, Mitchell Larson
 * @date 10/11/2017
 */


#include <stdint.h>
#include "AudioInterface.h"
#include "mainPlayBack.h"

#define SAMPLING_RATE (11025)
#define NUMBER_OF_CHANNELS (2)
#define BYTES_PER_SAMPLE (2)

/**
 * Adjusts the volume of 16 bit chunks of data in buffer pointer. Loops through buffer shifting the bits
 * to the left or right based on the gain. This is equivalent to taking each 16 bit chunk, and multiplying
 * it by 2^gain
 * @param bufferIn - buffer to be read from
 * @param bufferLength - length of the buffer
 * @param gain - and integer representing the volume adjustment
 */
void* adjustGain(void* bufferIn, int bufferLength, int gain) {
	if (bufferIn != NULL) {

		int16_t * buffer = bufferIn;

		//for the size of buffer/2 (because the int16 buffer will have half as many elements)
		for (int i = 0; i < bufferLength / 2; i++) {

			//adjust 16 bit chunks according to gain
			if (gain >= 0) {
				buffer[i] = buffer[i] << gain;
			} else {
				buffer[i] = buffer[i] >> (-1 * gain);
			}
		}

		return buffer;
	}
	return NULL;
}

/**
 * Initializes audio player to play sound. Sound information is read in from a pipe, send to
 * adjustGain where it increases or decreases the volume. The adjusted buffer is then used to send
 * data to the audio card.
 * @param volumeAdjustment - Volume adjustment. Valid range is -4 to 4
 * @param pAudioDeviceName - Name of device to play audio on
 * @param pipeToReadFrom - pipe identifier to read from
 */
void playAudio(int volumeAdjustment, char* pAudioDeviceName, int pipeToReadFrom){

	if (pAudioDeviceName != NULL) {
		// Instantiate and open a new audio interface.
		initializeAudioInterface(pAudioDeviceName, SAMPLING_RATE,
				NUMBER_OF_CHANNELS, SND_PCM_STREAM_PLAYBACK);

		// Open the audio interface.
		openAudioInterface();

		//not using BUFFER_SIZE or anything because its convenient for now
		char *buffer = (char*) malloc(BUFFER_SIZE);
		memset(&buffer[0], 0, BUFFER_SIZE);

		//used as size of bytes read in
		int rc;

		//read buffers until no more
		while (read(pipeToReadFrom, buffer, BUFFER_SIZE)) {

			//read size
			read(pipeToReadFrom, &rc, sizeof(int));

			//adjust buffer
			adjustGain(buffer, BUFFER_SIZE, volumeAdjustment);

			//write to sound thing
			writeToAudioInterface(buffer, rc);

			//reset buffer
			memset(&buffer[0], 0, BUFFER_SIZE);
		}

		closeAudioInterface();
		shutdownAudioInterface();

		close(pipeToReadFrom);

		free(buffer);
	}

	exit(0);

}


