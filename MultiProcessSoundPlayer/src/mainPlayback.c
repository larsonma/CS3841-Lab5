/**
 *Application playback
 * This program will play back a given audio file using the AudioInterface class and 2 child processes.
 * @author Mitchell Larson, Gunther Huebler
 * @file mainPlayback.c
 * @date 10/11/2017
 */

#include "FileReader.h"
#include "AudioInterface.h"
#include <sys/wait.h>
#include <unistd.h>


/**
 * This is the main method of the program.
 * @param argc This is the number of arguments passed in.  Must be 4 for the program to work.
 * @param argv These are the arguments passed in.
 * 			[0] - ./MultiProcessSoundPlayer
 * 			[1] - playback device
 * 			[2] - path to audio file
 * 			[3] - volume adjustment. Valid range is -4 to 4.
 * @return The return will be EXIT_SUCCESS if the program exits properly.
 */
int main(int argc, char* argv[]) {

	// Check to make certain the argument6 length is correct and usable.
	if (argc != 4) {
		printf("Usage: %s <playback device (i.e. plughw:0)> <File to playback> <Volume Adjustment -4 to 4>",
				argv[0]);
		exit(0);
	}

	int volumeAdjust = atoi(argv[3]);

	if(volumeAdjust > 4 || volumeAdjust < -4) {
		printf("Volume needs to be between -4 and 4\n");
		exit(0);
	}

	//fork off a new process for reading the contents of the binary file.
	pid_t pid;

	int cToCPipes[2];

	pipe(&cToCPipes[0]);

	if((pid = fork()) < 0){
		printf("Error creating child process\r\n");
		exit(0);
	}else if(pid == 0){
		//child process
		close(cToCPipes[0]);
		readFile(argv[2], cToCPipes[1]);
	}

	//fork off new process for playing audio
	if((pid = fork()) < 0){
		printf("Error creating child process\r\n");
		exit(0);
	}else if(pid == 0){
		//child process
		close(cToCPipes[1]);
		playAudio(volumeAdjust, argv[1], cToCPipes[0]);
	}else{

		close(cToCPipes[0]);
		close(cToCPipes[1]);
		//parent process
		waitpid(-1, NULL, 0);
	}

	return EXIT_SUCCESS;
}

