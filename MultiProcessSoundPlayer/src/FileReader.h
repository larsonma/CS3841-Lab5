/**
 * This file contains the interface FileReading child process. The following function causes the process to start
 * reading from a binary audio file.
 * @file FileReader.h
 * @author Mitchell Larson, Gunther Huebler
 * @date 10/11/2017
 */

#ifndef SRC_FILEREADER_H_
#define SRC_FILEREADER_H_

/**
 * Reads in a binary audio file in chunks of 2048 bytes. As data is read, it is sent through a pipe to a seperate child
 * process that will perform volume adjustments and send the pipe to the audio player architecture. The number of bytes
 * read is printed to the console every time the file is read. This function will kick off the audio playing sequence and
 * run until the entire is read.
 * @param fileNameToRead	non-null pointer to a fileName
 * @param pipeToSendTo		The pipe to send audio data through
 */
void readFile(char* fileNameToRead, int pipeToSendTo);

#endif /* SRC_FILEREADER_H_ */
