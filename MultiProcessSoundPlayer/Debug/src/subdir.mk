################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/AudioInterface.c \
../src/AudioPlayer.c \
../src/FileReader.c \
../src/mainPlayback.c 

OBJS += \
./src/AudioInterface.o \
./src/AudioPlayer.o \
./src/FileReader.o \
./src/mainPlayback.o 

C_DEPS += \
./src/AudioInterface.d \
./src/AudioPlayer.d \
./src/FileReader.d \
./src/mainPlayback.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


