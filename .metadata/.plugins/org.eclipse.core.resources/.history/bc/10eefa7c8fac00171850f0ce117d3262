/**
 *Application playback
 * This program will play back a given audio file using the AudioInterface class.
 * @author Walter Schilling
 * @date April, 2015
 */

#include "AudioInterface.h"
#include "FileReader.h"
#include <sys/wait.h>
#include <unistd.h>

#define SAMPLING_RATE (11025)
#define NUMBER_OF_CHANNELS (2)
#define BYTES_PER_SAMPLE (2)

/**
 * This is the main method of the program.
 * @param argc This is the number of arguments passed in.  Must be 3 for the program to work.
 * @param argv These are the arguments passed in.
 * @return The return will be EXIT_SUCCESS if the program exits properly.
 */
int main(int argc, char* argv[]) {
	int rc;
	int totalBytesRead = 0;

	// Check to make certain the argument6 length is correct and usable.
	if (argc != 3) {
		printf("Usage: %s <playback device (i.e. plughw:0)> <File to playback>",
				argv[0]);
		exit(0);
	}
	// Instantiate and open a new audio interface.
	initializeAudioInterface(argv[1], SAMPLING_RATE, NUMBER_OF_CHANNELS,
			SND_PCM_STREAM_PLAYBACK);

	// Open the audio interface.
	openAudioInterface();
	///////////////////////////////////////////////////////////////////

	//fork off a new process for reading the contents of the binary file.
	pid_t pid;
	int pToCPipes[2];
	pipe(&pToCPipes[0]);

	if((pid = fork()) < 0){
		printf("Error creating child process\r\n");
		exit(0);
	}else if(pid == 0){
		//child process
		readFile(argv[2], pToCPipes[1]);
	}else{
		//parent process
		waitpid(-1, NULL, 0);
	}





	// Allocate a buffer so that we can read data in from the file.  Initialize it to all zeros.
	buffer = (char*) malloc(BUFFER_SIZE);
	memset(&buffer[0], 0, BUFFER_SIZE);

	// Open the file that is going to be read.
	int filedesc = open(argv[2], O_RDONLY);
	rc = 1;

	do {
		// Read from the file.
		rc = read(filedesc, buffer, BUFFER_SIZE);
		totalBytesRead += rc;
		printf("Total Bytes Read: %d\n", totalBytesRead);

		// If we have read anything, write it out to the audio device.
		if (rc > 0) {
			writeToAudioInterface(buffer, rc);

			// Zero out the buffer.
			memset(&buffer[0], 0, BUFFER_SIZE);
		}
	} while (rc > 0);


	return EXIT_SUCCESS;
}

