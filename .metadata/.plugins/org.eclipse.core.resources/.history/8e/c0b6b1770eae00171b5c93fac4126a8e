/**
 *Application playback
 * This program will play back a given audio file using the AudioInterface class.
 * @author Walter Schilling
 * @date April, 2015
 */

#include "AudioInterface.h"
#include "FileReader.h"
#include <sys/wait.h>
#include <unistd.h>

#define SAMPLING_RATE (11025)
#define NUMBER_OF_CHANNELS (2)
#define BYTES_PER_SAMPLE (2)

/**
 * This is the main method of the program.
 * @param argc This is the number of arguments passed in.  Must be 3 for the program to work.
 * @param argv These are the arguments passed in.
 * @return The return will be EXIT_SUCCESS if the program exits properly.
 */
int main(int argc, char* argv[]) {

	// Check to make certain the argument6 length is correct and usable.
	if (argc != 4) {
		printf("Usage: %s <playback device (i.e. plughw:0)> <File to playback> <Volume Adjustment -4 to 4>",
				argv[0]);
		exit(0);
	}
	// Instantiate and open a new audio interface.
	initializeAudioInterface(argv[1], SAMPLING_RATE, NUMBER_OF_CHANNELS,
			SND_PCM_STREAM_PLAYBACK);

	// Open the audio interface.
	openAudioInterface();
	///////////////////////////////////////////////////////////////////

	//TODO Implement a second child process which handles playing audio and adjusting gain

	int volumeAdjust = atoi(argv[3]);

	if(volumeAdjust > 4 || volumeAdjust < -4) {
		printf("Volume needs to be between -4 and 4\n");
		exit(0);
	}

	//fork off a new process for reading the contents of the binary file.
	pid_t pid;

	int cToCPipes[2];

	pipe(&cToCPipes[0]);

	if((pid = fork()) < 0){
		printf("Error creating child process\r\n");
		exit(0);
	}else if(pid == 0){
		//child process
		readFile(argv[2], cToCPipes[1]);
	}

	if((pid = fork()) < 0){
		printf("Error creating child process\r\n");
		exit(0);
	}else if(pid == 0){
		//child process
	}else{
		//parent process
		waitpid(-1, NULL, 0);
	}

	return EXIT_SUCCESS;
}

